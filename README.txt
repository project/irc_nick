
Normally Drupal allows a wide range of characters in user names, like space and
foreign characters such as "Å", "Ŋ" and "Œ". The IRC Nick module will limit the
allowed user name characters to the classic old IRC characters.

The classic IRC characters are from A to Z, 0 to 9, and some other signs such
as caret ("^") and brackets ("[]").

This module is great when integrating IRC with a Drupal installation, as you
can then use the same names in both IRC and Drupal. I recommend authenticating
with the site for best results - I have a service under active development
for this. 
http://usoog.git.sourceforge.net/git/gitweb.cgi?p=usoog/IRCServices;a=summary

This module was initially written for http://usoog.com/
